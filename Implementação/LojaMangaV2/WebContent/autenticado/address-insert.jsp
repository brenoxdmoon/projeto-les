<%@page import="modelo.dominio.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Loja de Manga - Adicionar Endere�o</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>
<body>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo" src="assets/img/book-icon.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="<%=request.getContextPath()%>/index.jsp">Inicio</a></li>
                                    <li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li> <a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
                                <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <main>
        <!--================register_part Area =================-->
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-8 col-md-8">
					<h3 class="mb-30">Editar Endere�o de Cliente</h3>
					
					
					<form action="../SalvarEndereco" method="post">
						<div class="container">
							<div class="row align-items-center">
								<div class="col-lg-8 col-md-8">
								
									<%
										Cliente cli = (Cliente) session.getAttribute("clienteLogado");
									%>
									<h3 class="mb-30">Cadastro</h3>
									<div class="mt-10">
										<input type="hidden" id="idCli" name="idCli" value="<%=cli.getId()%>">
										
										<input type="text" id="txtLogradouro" name="txtLogradouro" placeholder="Logradouro"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Logradouro'" required
											class="single-input">
										<input type="text" id="txtComplemento" name="txtComplemento" placeholder="Complemento"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Complemento'" required
											class="single-input">
										<input type="text" id="txtCidade" name="txtCidade" placeholder="Cidade"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Cidade'" required
											class="single-input">
										<input type="text" id="txtCep" name="txtCep" placeholder="CEP"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'CEP'" required
											class="single-input">
										<input type="text" id="txtNumero" name="txtNumero" placeholder="Numero"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Numero'" required
											class="single-input">
										<input type="text" id="txtBairro" name="txtBairro" placeholder="Bairro"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Bairro'" required
											class="single-input">
										<div class="input-group-icon mt-10">
											<select class="form-select" id="selectTipoEnd" name="selectTipoEnd">
												<option selected="selected" value="Tipo">Tipo</option>
												<option value="RESIDENCIA">Resid�ncia</option>
												<option value="COBRANCA">Cobran�a</option>
												<option value="ENTREGA">Entrega</option>
											</select>
										</div>
										<div class="input-group-icon mt-10">
											<select class="form-select" id="selectEstado" name="selectEstado">
												<option selected="selected" value="Estado">Estado</option>
												<option value="AC">AC</option>
												<option value="AL">AL</option>
												<option value="AP">AP</option>
												<option value="AM">AM</option>
												<option value="BA">BA</option>
												<option value="CE">CE</option>
												<option value="ES">ES</option>
												<option value="DF">DF</option>
												<option value="GO">GO</option>
												<option value="MA">MA</option>
												<option value="MT">MT</option>
												<option value="MS">MS</option>
												<option value="MG">MG</option>
												<option value="PA">PA</option>
												<option value="PB">PB</option>
												<option value="PR">PR</option>
												<option value="PE">PE</option>
												<option value="PI">PI</option>
												<option value="RJ">RJ</option>
												<option value="RN">RN</option>
												<option value="RS">RS</option>
												<option value="RO">RO</option>
												<option value="RR">RR</option>
												<option value="SC">SC</option>
												<option value="SP">SP</option>
												<option value="SE">SE</option>
												<option value="TO">TO</option>
											</select>
										</div>
									</div>
									
									<div class="mt-10">
										<button class="btn_3" type="submit" id="OPERACAO" name="OPERACAO" value="SALVAR">Salvar</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>		
			</div>
		</div>
        <!--================register_part end =================-->
    </main>
    <footer>

    </footer>
    <!--? Search model Begin -->
    <!-- Search model end -->
    
    <!-- JS here -->

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    
    <!-- Scroll up, nice-select, sticky -->
    <script src="./assets/js/jquery.scrollUp.min.js"></script>
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- contact js -->
    <script src="./assets/js/contact.js"></script>
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>

</body>
    
</html>