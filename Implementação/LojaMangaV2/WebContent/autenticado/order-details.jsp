<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Loja de Mangá - Checkout de Pedido</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

  <!-- CSS here -->
      <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>

<body>

  <header>
    <!-- Header Start -->
    <div class="header-area">
        <div class="main-header header-sticky">
            <div class="container-fluid">
                <div class="menu-wrapper">
                    <!-- Logo -->
                    <div class="logo">
                      <a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo" src="assets/img/book-icon.png" alt=""></a>
                    </div>
                    <!-- Main-menu -->
                    <div class="main-menu d-none d-lg-block">
                        <nav>                                                
                          <ul id="navigation">  
                            <li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
                            <li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
                            <li><a href="#">P�ginas</a>
                                <ul class="submenu">
                                    <li><a href="<%=request.getContextPath()%>/login.jsp">Login</a></li>
                                    <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp">Carrinho</a></li>
                                    <li><a href="<%=request.getContextPath()%>/autenticado/confirmation.jsp">Confirma��o</a></li>
                                    <li><a href="<%=request.getContextPath()%>/autenticado/checkout.jsp">Checkout</a></li>
                                </ul>
                            </li>
                          </ul>
                        </nav>
                    </div>
                    <!-- Header Right -->
                    <div class="header-right">
                        <ul>
                            <li> <a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
                            <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span class="flaticon-shopping-cart"></span></a> </li>
                        </ul>
                    </div>
                </div>
                <!-- Mobile Menu -->
                <div class="col-12">
                    <div class="mobile_menu d-block d-lg-none"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
  </header>
    <main>
        <section class="checkout_area pt-5">
          <div class="container">            
            <div class="billing_details">
              <div class="row">
                <div class="col-lg-6">
                  <h3></h3>
                  <form class="row contact_form" action="#" method="post" novalidate="novalidate">
                    <div class="col-md-12 form-group p_star">
                      <select class="country_select">
                        <option value="1">Status do pedido</option>
                        <option value="2">Pendente</option>
                        <option value="3">Recusado</option>
                        <option value="4">Em Processo</option>
                        <option value="5">Em transporte</option>
                        <option value="6">Entregue</option>
                      </select>
                    </div>          
                  </form>
                </div>
                <div class="col-lg-6">
                  <div class="order_box">
                    <h2>Seu Pedido</h2>
                    <ul class="list">
                      <li>
                        <a href="#">Produto
                          <span>Total</span>
                        </a>
                      </li>
                      <li>
                        <a href="#">One Piece - Volume 95
                          <span class="middle">x 01</span>
                          <span class="last">R$ 19.50</span>
                        </a>
                      </li>
                      <li>
                        <a href="#">Attack on Titan - Volume 13
                          <span class="middle">x 01</span>
                          <span class="last">R$ 19.50</span>
                        </a>
                      </li>                      
                    </ul>
                    <ul class="list list_2">
                      <li>
                        <a href="#">Subtotal
                          <span>R$ 39.00</span>
                        </a>
                      </li>
                      <li>
                        <a href="#">Frete
                          <span>Correio: $50.00</span>
                        </a>
                      </li>
                      <li>
                        <a href="#">Total
                          <span>R$ 39.00</span>
                        </a>
                      </li>
                    </ul>
                    <a class="btn_3" href="admin-order.html">Confirmar Atualização</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--================End Checkout Area =================-->
    </main>
    <!--? Search model Begin -->
    <div class="search-model-box">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-btn">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Searching key.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

<!-- JS here -->

  <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
  <!-- Jquery, Popper, Bootstrap -->
  <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
  <script src="./assets/js/popper.min.js"></script>
  <script src="./assets/js/bootstrap.min.js"></script>
  <!-- Jquery Mobile Menu -->
  <script src="./assets/js/jquery.slicknav.min.js"></script>

  <!-- Jquery Slick , Owl-Carousel Plugins -->
  <script src="./assets/js/owl.carousel.min.js"></script>
  <script src="./assets/js/slick.min.js"></script>

  <!-- One Page, Animated-HeadLin -->
  <script src="./assets/js/wow.min.js"></script>
  <script src="./assets/js/animated.headline.js"></script>
  <script src="./assets/js/jquery.magnific-popup.js"></script>

  <!-- Scroll up, nice-select, sticky -->
  <script src="./assets/js/jquery.scrollUp.min.js"></script>
  <script src="./assets/js/jquery.nice-select.min.js"></script>
  <script src="./assets/js/jquery.sticky.js"></script>
  
  <!-- contact js -->
  <script src="./assets/js/contact.js"></script>
  <script src="./assets/js/jquery.form.js"></script>
  <script src="./assets/js/jquery.validate.min.js"></script>
  <script src="./assets/js/mail-script.js"></script>
  <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
      
  <!-- Jquery Plugins, main Jquery -->	
  <script src="./assets/js/plugins.js"></script>
  <script src="./assets/js/main.js"></script>
  
</body>
</html>