<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Cadastrar Produto</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">

<!-- Script here -->

</head>
<body>
	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo"
								src="assets/img/book-icon.png" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
									<li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
								</ul>
							</nav>
						</div>
						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<li><a href="<%=request.getContextPath()%>/login.jsp"><span
										class="flaticon-user"></span></a></li>
								<li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span
										class="flaticon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
					<!-- Mobile Menu -->
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
	</header>
	<main>
		<!--================register_part Area =================-->
		<form action="../../SalvarManga" method="post">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-8 col-md-8">
						<h3 class="mb-30">Cadastro</h3>

						<div class="mt-10">
							<input type="text" name="txtTitulo" id="txtTitulo"
								placeholder="T�tulo" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'T�tulo'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtAutor" id="txtAutor"
								placeholder="Autor" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Autor'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtAno" id="txtAno" placeholder="Ano"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Ano'" required class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtEditora" id="txtEditora"
								placeholder="Editora" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Editora'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtEdicao" id="txtEdicao"
								placeholder="Edi��o" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Edi��o'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtIsbn" id="txtIsbn" placeholder="ISBN"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'ISBN'" required class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtCodigoBarras" id="txtCodigoBarras"
								placeholder="C�digo de barras" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'C�digo de barras'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtPaginas" id="txtPaginas"
								placeholder="N�mero de P�ginas" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'N�mero de P�ginas'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<textarea class="single-textarea" placeholder="Sin�pse"
								name="txtSinopse" id="txtSinopse"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Sin�pse'" required></textarea>
						</div>

						<div class="mt-10">
							<input type="text" name="txtPrecificacao" id="txtPrecificacao"
								placeholder="Grupo de Pecifica��o"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Grupo de Precifica��o'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtQuantidade" id="txtQuantidade"
								placeholder="Quantidade" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Quantidade'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtPreco" id="txtPreco"
								placeholder="Pre�o" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Quantidade'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
										<center>
											<h5>Dimens�es</h5>
										</center>
										<input type="text" name="txtAltura" id="txtAltura"
											placeholder="Altura" onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Altura'" required
											class="single-input"> <input type="text"
											name="txtLargura" id="txtLargura" placeholder="Largura"
											onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Largura'" required
											class="single-input">
											<input type="text"
											name="txtProfundidade" id="txtProfundidade"
											placeholder="Profundidade" onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Profundidade'" required
											class="single-input">
											<input type="text"name="txtPeso" id="txtPeso"
											placeholder="Peso" onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Peso'" required
											class="single-input">
									</blockquote>
								</div>
							</div>
						</div>

						<div class="mt-10">
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
										<center>
											<h5>Categorias</h5>
										</center>
										<div class="single-element-widget mt-30">

											<div class="switch-wrap d-flex justify-content-between">
												<p>Categoria 1</p>
												<div class="primary-checkbox">
													<input type="checkbox" id="cate1"> <label
														for="cate1"></label>
												</div>
											</div>

											<div class="switch-wrap d-flex justify-content-between">
												<p>Categoria 2</p>
												<div class="primary-checkbox">
													<input type="checkbox" id="cate2"> <label
														for="cate2"></label>
												</div>
											</div>

										</div>
									</blockquote>
									
								</div>
							</div>
						</div>

						<div class="mt-10">
							<input type="file" class="form-control" accept="image/*"
										name="mgImagem" id="mgImagem">
						</div>

						<div class="mt-10 my-5">
							<button type="submit" id="OPERACAO" name="OPERACAO"
								value="SALVAR" class="btn_3" href="user-profile.html">
								Registrar</button>
						</div>

					</div>
				</div>
			</div>
		</form>
		<!--================register_part end =================-->
	</main>
	<footer> </footer>
	<!--? Search model Begin -->
	<!-- Search model end -->

	<!-- JS here -->

	<script src="./assets/js/funcoes.js"></script>

	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>

	<!-- Scroll up, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<!-- JQuery Mask -->
	<script type="text/javascript"
		src="assets/js/mask/dist/jquery.mask.min.js"></script>
	<script type="text/javascript" src="assets/js/mask/dist/aplica.js"></script>
</body>

</html>