
<%@page import="modelo.dominio.EntidadeDominio"%>
<%@page import="modelo.dominio.Pedido"%>
<%@page import="modelo.dao.PedidoDAO"%>
<%@page import="modelo.dominio.Resultado"%>
<%@page import="modelo.dominio.Manga"%>
<%@page import="modelo.dao.MangaDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Gr�fico</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="manifest" href="site.webmanifest">
		<link rel="shortcut icon" type="image/x-icon"
			href="assets/img/favicon.ico">
		<!-- CSS here -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/flaticon.css">
		<link rel="stylesheet" href="../assets/css/slicknav.css">
		<link rel="stylesheet" href="../assets/css/animate.min.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css">
		<link rel="stylesheet" href="../assets/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="../assets/css/themify-icons.css">
		<link rel="stylesheet" href="../assets/css/slick.css">
		<link rel="stylesheet" href="../assets/css/nice-select.css">
		<link rel="stylesheet" href="../assets/css/style.css">
	</head>
	
	<% 
	
		Manga mg = new Manga();
		mg.setId(-1);
		MangaDAO mgDao = new MangaDAO();
		Resultado resultManga = mgDao.consultar(mg);
		
		Pedido ped = new Pedido();
		ped.setStatus("todospedidos");
		PedidoDAO pedDao = new PedidoDAO();
		Resultado resultPedido = pedDao.consultar(ped);
		
		int mangasVendidos;
		
		//Map<Int, >
		
		for(EntidadeDominio e : resultPedido.getEntidades()){
			
		}
	
	%>
	
	<body>
		<div class="section-top-border">
			<h3 class="mb-30">Table</h3>
			<div class="progress-table-wrap">
				<div class="progress-table">
					<div class="table-head">
					
						<div class="serial">#</div>
						<div class="country">Mang�</div>
						<div class="visit">qtde</div>
						<div class="percentage">progressao</div>
						
					</div>
					
					<% 
					for(EntidadeDominio e : resultManga.getEntidades()){
						Manga mgEnt = (Manga) e;
					
					%>
					
					<div class="table-row">
						<div class="serial"><%=mgEnt.getId() %></div>
						<div class="country">
							<img src="../../assets/img/assets/img/<%=mgEnt.getEndereco_imagem() %>" alt="flag"><%=mg.getTitulo() %>
						</div>
						<div class="visit">645032</div>
						<div class="percentage">
							<div class="progress">
								<div class="progress-bar color-1" role="progressbar"
									style="width: 80%" aria-valuenow="80" aria-valuemin="0"
									aria-valuemax="100"></div>
							</div>
						</div>
					</div>
					
					<% 
					}
					%>
				</div>
			</div>
		</div>
	</body>
	
	<footer>
		<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
		<!-- Jquery, Popper, Bootstrap -->
		<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
		<script src="./assets/js/popper.min.js"></script>
		<script src="./assets/js/bootstrap.min.js"></script>
		<!-- Jquery Mobile Menu -->
		<script src="./assets/js/jquery.slicknav.min.js"></script>
	
		<!-- Jquery Slick , Owl-Carousel Plugins -->
		<script src="./assets/js/owl.carousel.min.js"></script>
		<script src="./assets/js/slick.min.js"></script>
	
		<!-- One Page, Animated-HeadLin -->
		<script src="./assets/js/wow.min.js"></script>
		<script src="./assets/js/animated.headline.js"></script>
		<script src="./assets/js/jquery.magnific-popup.js"></script>
	
		<!-- Scroll up, nice-select, sticky -->
		<script src="./assets/js/jquery.scrollUp.min.js"></script>
		<script src="./assets/js/jquery.nice-select.min.js"></script>
		<script src="./assets/js/jquery.sticky.js"></script>
	
		<!-- contact js -->
		<script src="./assets/js/contact.js"></script>
		<script src="./assets/js/jquery.form.js"></script>
		<script src="./assets/js/jquery.validate.min.js"></script>
		<script src="./assets/js/mail-script.js"></script>
		<script src="./assets/js/jquery.ajaxchimp.min.js"></script>
	
		<!-- Jquery Plugins, main Jquery -->
		<script src="./assets/js/plugins.js"></script>
		<script src="./assets/js/main.js"></script>
	</footer>
	
</html>