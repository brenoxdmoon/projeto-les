<!-- Funções de Endereco -->
		function mostrarEnd(){
			var elementDiv = document.getElementById("escondedorEndDiv");
			elementDiv.innerHTML = "<div class='mt-10'><div class='row'><div class='col-lg-12'><blockquote class='generic-blockquote'><center><h5>Endereco</h5></center><div class='input-group-icon mt-10'><div class='form-select' id='default-select'><select><option selected='selected' value='1'>Pais</option><option value='2'>Brasil</option><option value='3'>Outro</option></select></div></div><input type='text' name='estado' placeholder='Estado'onfocus='this.placeholder = ''' onblur='this.placeholder = 'Estado'' required class='single-input'><input type='text' name='cidade' placeholder='Cidade'onfocus='this.placeholder = ''' onblur='this.placeholder = 'Cidade'' required class='single-input'><input type='text' name='bairro' placeholder='Bairro'onfocus='this.placeholder = ''' onblur='this.placeholder = 'Bairro'' required class='single-input'><input type='text' name='rua' placeholder='Rua'onfocus='this.placeholder = ''' onblur='this.placeholder = 'Rua'' required class='single-input'><input type='text' name='numero' placeholder='Número'onfocus='this.placeholder = ''' onblur='this.placeholder = 'Número'' required class='single-input'><input type='text' name='cep' placeholder='CEP'onfocus='this.placeholder = ''' onblur='this.placeholder = 'CEP'' required class='single-input'></blockquote></div></div></div>";
			
			var elementBtn = document.getElementById("escondedorEndBtn");
			elementBtn.innerHTML = "Cancelar novo Endereco"
			elementBtn.setAttribute("onclick", "esconderEnd();")
			elementBtn.setAttribute("class", "genric-btn danger radius")
		}
		
		function esconderEnd(){
			var elementDiv = document.getElementById("escondedorEndDiv");
			elementDiv.innerHTML = "";
			
			var elementBtn = document.getElementById("escondedorEndBtn");
			elementBtn.innerHTML = "Adicionar outro Endereco"
			elementBtn.setAttribute("onclick", "mostrarEnd();")
			elementBtn.setAttribute("class", "genric-btn info radius")
		}
		
		
		<!-- Funções de Telefone -->
		function mostrarTel(){
			var elementDiv = document.getElementById("escondedorTelDiv");
			elementDiv.innerHTML = "<div class='mt-10'>	<div class='row'> <div class='col-lg-12'> <blockquote class='generic-blockquote'> <center><h5>Telefone</h5></center> <div class='input-group-icon mt-10'> <div class='form-select' id='default-select'> <select> <option selected='selected' value='1'>Tipo</option> <option value='2'>Residencial</option> <option value='3'>Celular</option> </select> </div> </div> <input type='number' name='ddd' placeholder='DDD' onfocus='this.placeholder = ''' onblur='this.placeholder = 'DDD'' required class='single-input'> <input type='number' name='numero' placeholder='Número' onfocus='this.placeholder = ''' onblur='this.placeholder = 'Número'' required class='single-input'> </blockquote> </div> </div> </div>";
			
			var elementBtn = document.getElementById("escondedorTelBtn");
			elementBtn.innerHTML = "Cancelar novo Telefone"
			elementBtn.setAttribute("onclick", "esconderTel();")
			elementBtn.setAttribute("class", "genric-btn danger radius")
		}
		
		function esconderTel(){
			var elementDiv = document.getElementById("escondedorTelDiv");
			elementDiv.innerHTML = "";
			
			var elementBtn = document.getElementById("escondedorTelBtn");
			elementBtn.innerHTML = "Adicionar outro Endereco"
			elementBtn.setAttribute("onclick", "mostrarTel();")
			elementBtn.setAttribute("class", "genric-btn info radius")
		}
		
		
		<!-- Funções de Cartão -->
		function mostrarCrt(){
			var elementDiv = document.getElementById("escondedorCrtDiv");
			elementDiv.innerHTML = "<div class='mt-10'> <div class='row'> <div class='col-lg-12'> <blockquote class='generic-blockquote'> <center><h5>Cartão de Credito</h5></center> <input type='number' name='numeroCartao' placeholder='Número do cartão' onfocus='this.placeholder = ''' onblur='this.placeholder = 'Número do cartão'' required class='single-input'> <input type='text' name='nomeCartao' placeholder='Nome no cartão' onfocus='this.placeholder = ''' onblur='this.placeholder = 'Nome no cartão'' required class='single-input'> <input type='text' name='validadeCartao' placeholder='Validade do cartão' onfocus='this.placeholder = ''' onblur='this.placeholder = 'Validade do cartão'' required class='single-input'> </blockquote> </div> </div> </div>";
			
			var elementBtn = document.getElementById("escondedorCrtBtn");
			elementBtn.innerHTML = "Cancelar novo Cartão de Credito"
			elementBtn.setAttribute("onclick", "esconderCrt();")
			elementBtn.setAttribute("class", "genric-btn danger radius")
		}
		
		function esconderCrt(){
			var elementDiv = document.getElementById("escondedorCrtDiv");
			elementDiv.innerHTML = "";
			
			var elementBtn = document.getElementById("escondedorCrtBtn");
			elementBtn.innerHTML = "Adicionar outro Cartão de Credito"
			elementBtn.setAttribute("onclick", "mostrarCrt();")
			elementBtn.setAttribute("class", "genric-btn info radius")
		}