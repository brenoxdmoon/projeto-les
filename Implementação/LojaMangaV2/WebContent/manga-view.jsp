<%@page import="modelo.dominio.Cliente"%>
<%@page import="modelo.dominio.Manga"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Watch shop | eCommers</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">

</head>


<body>

	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo"
								src="assets/img/book-icon.png" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="<%=request.getContextPath()%>/index.jsp">Inicio</a></li>
									<li><a href="<%=request.getContextPath()%>/shop.html">Produtos</a></li>
								</ul>
							</nav>
						</div>
						<!-- Mobile Menu -->

					</div>
				</div>
			</div>
			<!-- Header End -->
	</header>

	<%
		if (session.getAttribute("clienteLogado") != null) {
			Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
			session.setAttribute("clienteLogado", cliente);
			
		}else{
			Cliente cliente = new Cliente();
			cliente.setNomeCompleto("clienteDeslogado");
			session.setAttribute("clienteLogado", cliente);
		}

	Manga manga = (Manga) session.getAttribute("VisualizarMg");
	%>
	<main>
		<!--================Single Product Area =================-->
		<div class="product_image_area">
			<form action="AdicionarItemCarrinho" method="post">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-12">
							<img src="assets/img/<%=manga.getEndereco_imagem()%>" alt="#"
								class="img-fluid">
							<div class="col-lg-8">
								<div class="single_product_text text-center">
									<h3>
										<%=manga.getTitulo()%>
									</h3>
									<p>
										Sinopse:
										<%=manga.getSinopse()%></p>
										<p>Quantidade em estoque: <%= manga.getQuantidade()%></p>
									<div class="card_area">
										<div class="product_count_area">
											<p>Quantidade</p>
											<div class="product_count d-inline-block">
												<span class="product_count_item inumber-decrement"> <i
													class="ti-minus"></i></span> <input
													class="product_count_item input-number" type="text"
													value="1" min="0" max="10" name="txtQntde" id="txtQntde">
												<span class="product_count_item number-increment"> <i
													class="ti-plus"></i></span>
											</div>
											<p>
												R$
												<%=manga.getPreco()%></p>
										</div>
										<div class="add_to_cart">
											<button name="OPERACAO" id="OPERACAO" value="SALVAR" class="btn_3">adicionar ao carrinho</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!--================End Single Product Area =================-->
	</main>
	<footer>
		
	</footer>

	<!-- JS here -->

	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scroll up, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<!-- swiper js -->
	<script src="./assets/js/swiper.min.js"></script>
	<!-- swiper js -->
	<script src="./assets/js/mixitup.min.js"></script>
	<script src="./assets/js/jquery.counterup.min.js"></script>
	<script src="./assets/js/waypoints.min.js"></script>

</body>

</html>