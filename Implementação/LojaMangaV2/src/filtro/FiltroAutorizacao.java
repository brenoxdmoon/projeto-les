package filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.dominio.Cliente;

/**
 * Servlet Filter implementation class FiltroAutorizacao
 */
@WebFilter("/FiltroAutorizacao")
public class FiltroAutorizacao implements Filter {

    /**
     * Default constructor. 
     */
    public FiltroAutorizacao() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest requestOriginal, ServletResponse responseOriginal, FilterChain chain) throws IOException, ServletException {
//		// Transforma as requests em HttpServletRequest
//        HttpServletRequest request = (HttpServletRequest) requestOriginal;
//        // Transforma as response em HttpServletRequest
//        HttpServletResponse response = (HttpServletResponse) responseOriginal;
//        // Recupera a session para verificar se existe um cliente autenticado
//        HttpSession session = request.getSession();
//        
//        System.out.println("- entrou no filtro");
//
//        if (request.getRequestURI().startsWith(request.getContextPath() + "/autenticado/admin")) {
//            if (session.getAttribute("clienteLogado") == null) {
//                // para pastas anteriores
//                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
//                // para o mesmo local
//                // request.getRequestDispatcher("/login.jsp").forward(requestOriginal, responseOriginal);
//            } else {
//                Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
//                if (cliente.getAdmin() == 0) {
//                    response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
//                } else {
//                    chain.doFilter(requestOriginal, responseOriginal);
//                }
//            }
//        } else if (request.getRequestURI().startsWith(request.getContextPath() + "/autenticado")) {
//            if (session.getAttribute("clienteLogado") == null) {
//                // para pastas anteriores
//                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
//                // para o mesmo local
//                // request.getRequestDispatcher("/login.jsp").forward(requestOriginal, responseOriginal);
//            } else {
//                Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
//                if (cliente.getAdmin() == 0) {
//                    chain.doFilter(requestOriginal, responseOriginal);
//                } else {
//                    response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/admin-profile.jsp"));
//                }
//            }
//        } else {
//            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
//        }
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
