package filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class FiltroAutenticacao
 */
@WebFilter("/FiltroAutenticacao")
public class FiltroAutenticacao implements Filter {

    /**
     * Default constructor. 
     */
    public FiltroAutenticacao() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest requestOriginal, ServletResponse responseOriginal, FilterChain chain) throws IOException, ServletException {
		// Transforma as requests em HttpServletRequest
        HttpServletRequest request = (HttpServletRequest) requestOriginal;
        // Transforma as response em HttpServletRequest
        HttpServletResponse response = (HttpServletResponse) responseOriginal;
        // Recupera a session para verificar se existe um cliente logado
        HttpSession session = request.getSession();
        System.out.println(request.getRequestURI());

        // Se for CSS, JS ou images ele ignora o filtro
        if (!request.getRequestURI().startsWith(request.getContextPath() + "/js")
                && !request.getRequestURI().startsWith(request.getContextPath() + "/css")
                && !request.getRequestURI().startsWith(request.getContextPath() + "/images")
                && null == session.getAttribute(("cliente"))) {
            // para pastas anteriores
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
            // para o mesmo local
            // request.getRequestDispatcher("/login.jsp").forward(requestOriginal, responseOriginal);
        } else {
            chain.doFilter(requestOriginal, responseOriginal);
        }
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
