package controle.strategy.cliente;

import controle.strategy.IStrategy;
import modelo.dominio.Cliente;
import modelo.dominio.EntidadeDominio;

public class ValidaPadraoSenha implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Cliente cliente = (Cliente)entidade;
        
        if (!cliente.getSenha().getSenha().matches("^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{8,}$")) {
            return ("A senha deve conter no minimo 8 caracteres, entre eles numeros e letras.");
        }
        return null;
    }
    
}
