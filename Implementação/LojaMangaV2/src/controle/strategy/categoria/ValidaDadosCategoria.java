package controle.strategy.categoria;


import java.text.ParseException;
import java.util.Date;

import controle.strategy.IStrategy;
import modelo.dominio.Categoria;
import modelo.dominio.EntidadeDominio;


public class ValidaDadosCategoria implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Categoria categoria = (Categoria) entidade;
        StringBuilder msg = new StringBuilder();

        if (categoria.getCategoria().equals("") || categoria.getCategoria() == null) {
            msg.append("O nome da categoria eh obrigatoria.");
        }

        return msg.toString();
    }

}
