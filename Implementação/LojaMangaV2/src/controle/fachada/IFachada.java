package controle.fachada;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public interface IFachada {

	public Resultado salvar(EntidadeDominio ent);
	public Resultado consultar(EntidadeDominio ent);
	public Resultado alterar(EntidadeDominio ent);
	public Resultado excluir(EntidadeDominio ent);
	public Resultado visualizar(EntidadeDominio entidade);
}
