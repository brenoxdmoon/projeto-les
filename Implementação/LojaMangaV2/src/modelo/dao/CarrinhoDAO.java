package modelo.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import modelo.dominio.Carrinho;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.Resultado;

public class CarrinhoDAO extends AbstractDAO{

	public CarrinhoDAO() {
		super("estoque", "etq_id");
	}

	//Apesar do Dao estar dito como "Carrinho", o Carrinho n�o � persistido em Banco.
	//Na verdade, � s� uma alegoria para dizer que estou bloqueando os produtos no Estoque(subtraindo a quantidade de produtos no estoque) para manter os produtos no carrinho.
	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		
		Carrinho carrinho = (Carrinho) entidadeDominio;
		Resultado resultado = new Resultado();
		EstoqueDAO etqDAO = new EstoqueDAO();
		
		for (Manga mg: carrinho.getMangas()) {
			etqDAO.alterar(mg);

		}
		
		resultado.add(carrinho);
		
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
		Resultado resultado = new Resultado();
		
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
		Resultado resultado = new Resultado();
		
		return resultado;
	}

}
