package modelo.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Estoque;
import modelo.dominio.Manga;
import modelo.dominio.Resultado;

public class EstoqueDAO extends AbstractDAO {

	public EstoqueDAO(String nomeTabela, String idTabela) {
		super(nomeTabela, idTabela);
		// TODO Auto-generated constructor stub
	}
	
	public EstoqueDAO() {
		super("estoque", "etq_id");
		
	}

	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();

		Manga mg = (Manga) entidadeDominio;
		
		Estoque estoque = new Estoque();
		
		Resultado resultado = new Resultado();
		
		estoque.setManga((Manga) entidadeDominio);
		estoque.setQuantidadeEstoque(estoque.getManga().getQuantidade());
		estoque.setAtivo("ATIVO");
		
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO " + nomeTabela +" (");
		sql.append("etq_mg_id, ");
		sql.append("etq_quantidade");
		sql.append("ativo");
		sql.append(") VALUES (?, ?, ?)");
		
		try {
			
			pst = conexao.prepareStatement(sql.toString());
			
			pst.setInt(1, estoque.getManga().getId());
			pst.setInt(2, estoque.getQuantidadeEstoque());
			pst.setString(3, "ATIVO");
			
			conexao.commit();
			
		}catch(SQLException e) {
			
		}
		
		return resultado;
	}
	
	
	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
		Manga mg = (Manga) entidadeDominio;
		PreparedStatement pst = null;
		
		Resultado resultado = new Resultado();
		StringBuilder sql = new StringBuilder();
		
		mg.setQuantidade(mg.getQuantidade() - mg.getQuantidadeProdutoCarrinho());
		
		sql.append("UPDATE " + nomeTabela + " SET ");
		sql.append("etq_quantidade = ? WHERE etq_mg_id = " + mg.getId());

		
		try {
			pst = conexao.prepareStatement(sql.toString());

			pst.setInt(1, mg.getQuantidade());
			pst.executeUpdate();
			
			conexao.commit();
			
			resultado.add(mg);
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			try {
				conexao.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		
		return null;
	}

}
