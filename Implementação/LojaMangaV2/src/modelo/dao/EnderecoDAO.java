package modelo.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.dominio.Cliente;
import modelo.dominio.Endereco;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.Resultado;


public class EnderecoDAO extends AbstractDAO{

    public EnderecoDAO(Connection conexao, String nomeTabela, String idTabela) {
        super(conexao, "endereco", "end_id");
    }

    public EnderecoDAO(String nomeTabela, String idTabela) {
        super("endereco", "end_id");
    }

    public EnderecoDAO() {
        super("endereco", "end_id");
    }

    @Override
    public Resultado salvar(EntidadeDominio entidadeDominio) {
    	
    	abrirConexao();
    	
    	Resultado resultado = new Resultado();
    	
        Endereco endereco = (Endereco)entidadeDominio;
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        
        System.out.println("EndDAO salvar Logradouro: " + endereco.getLogradouro());
        System.out.println("EndDAO salvar Numero: " + endereco.getNumero());
        System.out.println("EndDAO salvar Bairro: " + endereco.getBairro());
        System.out.println("EndDAO salvar Cep: " + endereco.getCep());
        System.out.println("EndDAO salvar Cidade: " + endereco.getCidade());
        System.out.println("EndDAO salvar Estado: " + endereco.getEstado());
        System.out.println("EndDAO salvar cli id: " + endereco.getCliente().getId());
        System.out.println("EndDAO salvar Tipo: " + endereco.getTipo());
        System.out.println("EndDAO salvar Complemento: " + endereco.getComplemento());
        System.out.println("EndDAO salvar Ativo: " + endereco.getAtivo());

        sql.append("INSERT INTO " + nomeTabela + " (");
        
        sql.append("end_logradouro, ");
        sql.append("end_numero, ");
        sql.append("end_bairro, ");
        sql.append("end_cep, ");
        sql.append("end_cidade, ");
        sql.append("end_estado, ");
        sql.append("FK_cli_id_end, ");
        sql.append("end_tipo, ");
        sql.append("end_complemento, ");
        sql.append("ativo");
        
        sql.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        
        try {
            pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, endereco.getLogradouro());
            pst.setString(2, endereco.getNumero());
            pst.setString(3, endereco.getBairro());
            pst.setString(4, endereco.getCep());
            pst.setString(5, endereco.getCidade());
            pst.setString(6, endereco.getEstado());
            pst.setInt	 (7, endereco.getCliente().getId());
            pst.setString(8, endereco.getTipo());
            pst.setString(9, endereco.getComplemento());
            pst.setString(10, endereco.getAtivo());
            
            pst.executeUpdate();
            conexao.commit();

            ResultSet rs = pst.getGeneratedKeys();
            int idEndereco = 0;
            if(rs.next()){
                idEndereco = rs.getInt(1);
            }
            endereco.setId(idEndereco);
            
            resultado.add(endereco);
            conexao.close();
        }catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
        System.out.println("PST Endere�o: " + pst);
		return resultado;
    }

    // Altera um endereco com base no endereco recebido por parametro
    @Override
    public Resultado alterar(EntidadeDominio entidadeDominio) {
    	
    	abrirConexao();
    	
    	Resultado resultado = new Resultado();
    	
        Endereco endereco;
        try {
        	endereco = (Endereco) entidadeDominio;
        } catch (ClassCastException e) {
        	endereco = new Endereco();
        	endereco.setAtivo("ATIVO");
        }
        
        System.out.println("EndDAO alterar: " + endereco.getLogradouro());
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        System.out.println("EnderecoDAO: alterar - End do cliente: " + endereco.getCliente());

        sql.append("UPDATE " + nomeTabela + " SET ");
        sql.append("end_logradouro = ?, ");
        sql.append("end_numero = ?, ");
        sql.append("end_bairro = ?, ");
        sql.append("end_cep = ?, ");
        sql.append("end_cidade = ?, ");
        sql.append("end_estado = ?, ");
        sql.append("FK_cli_id_end = ?, ");
        sql.append("end_tipo = ?, ");
        sql.append("end_complemento = ?, ");
        sql.append("ativo = ?");
        
        sql.append(" WHERE (" + idTabela + " = " + endereco.getId() + ");");
        
        try {
            pst = conexao.prepareStatement(sql.toString());

            pst.setString(1, endereco.getLogradouro());
            pst.setString(2, endereco.getNumero());
            pst.setString(3, endereco.getBairro());
            pst.setString(4, endereco.getCep());
            pst.setString(5, endereco.getCidade());
            pst.setString(6, endereco.getEstado());
            pst.setInt(7, endereco.getCliente().getId());
            pst.setString(8, endereco.getTipo());
            pst.setString(9, endereco.getComplemento());
            pst.setString(10, endereco.getAtivo());
            
            pst.executeUpdate();
            conexao.commit();

            System.out.println("- Alterou");				
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
		return resultado;
    }

    // Consulta enderecos
    @Override
    public Resultado consultar(EntidadeDominio entidadeDominio) {
    	
    	abrirConexao();
    	
    	Resultado resultado = new Resultado();
        Endereco endereco = (Endereco) entidadeDominio;
        
        System.out.println("EndDAO consultar: " + endereco.getLogradouro());
        
        ResultSet rs;
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        
        if(endereco.getId() != -1) {
        	sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
            sql.append(" WHERE end_id = " + endereco.getId());
        } else {
        	sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
        }
        
        try {
            pst = conexao.prepareStatement(sql.toString());
            
            rs = pst.executeQuery();	
            
            endereco = null;

            while(rs.next()){
                endereco = new Endereco(rs.getString("end_logradouro"), rs.getString("end_numero"),
                						rs.getString("end_complemento"), rs.getString("end_bairro"),
                						rs.getString("end_cidade"), rs.getString("end_estado"),
                						rs.getString("end_cep"), rs.getString("end_tipo"));
                endereco.setId(rs.getInt("end_id"));
                
                Cliente cliente = new Cliente();
                cliente.setId(rs.getInt("FK_cli_id_end"));
                endereco.setCliente(cliente);
                resultado.add(endereco);
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        return resultado;
    }
    
public Resultado consultarViaCliente(EntidadeDominio entidadeDominio) {
    	
    	abrirConexao();
    	
    	Resultado resultado = new Resultado();
        Cliente cli = (Cliente) entidadeDominio;
        
        System.out.println("EndDAO consultar cli: " + cli.getNomeCompleto());
        
        ResultSet rs;
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        
        if(cli.getId() != -1) {
        	sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
            sql.append(" WHERE FK_cli_id_end = " + cli.getId());
        } else {
        	sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
        }
        
        try {
            pst = conexao.prepareStatement(sql.toString());
            
            rs = pst.executeQuery();

            while(rs.next()){
                Endereco endereco = new Endereco(rs.getString("end_logradouro"), rs.getString("end_numero"),
                						rs.getString("end_complemento"), rs.getString("end_bairro"),
                						rs.getString("end_cidade"), rs.getString("end_estado"),
                						rs.getString("end_cep"), rs.getString("end_tipo"));
                endereco.setId(rs.getInt("end_id"));
                
                Cliente cliente = new Cliente();
                cliente.setId(rs.getInt("FK_cli_id_end"));
                endereco.setCliente(cliente);
                resultado.add(endereco);
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        return resultado;
    }
}
