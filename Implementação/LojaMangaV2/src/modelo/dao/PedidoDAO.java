package modelo.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.dominio.CartaoCredito;
import modelo.dominio.Cliente;
import modelo.dominio.Endereco;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.MangasPedido;
import modelo.dominio.Pedido;
import modelo.dominio.Resultado;

public class PedidoDAO extends AbstractDAO {

	public PedidoDAO(String nomeTabela, String idTable) {
		super("pedidos", "ped_id");
		// TODO Auto-generated constructor stub
	}

	public PedidoDAO() {
		super("pedidos", "ped_id");
	}

	 @Override
	    public Resultado salvar(EntidadeDominio entidadeDominio) {
		 
	        abrirConexao();

	        Resultado resultado = new Resultado();
	        PreparedStatement pst = null;
	        Pedido pedido = (Pedido) entidadeDominio;
	        MangaDAO mangaDao = new MangaDAO();
	        StringBuilder sql = new StringBuilder();

	        sql.append("INSERT INTO " + nomeTabela + "(");
	        sql.append("ped_cartao, ");
	        sql.append("ped_valor_total, ");
	        sql.append("ped_status, ");
	        sql.append("ped_data_pagamento, ");
	        sql.append("ped_data_pedido, ");
	        sql.append("ped_cli_id, ");
	        sql.append("ped_end_id, ");
	        sql.append("ativo");
	        sql.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

	        try {
	            pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);// retorna as chaves primarias geradas
	            
	            pst.setString(1, pedido.getCartao().getNomeImpresso());
	            pst.setDouble(2, pedido.getValorTotal());
	            pst.setString(3, "Em Processamento");
	            pst.setString(4, pedido.getDataPagamento());
	            pst.setString(5, pedido.getDataPedido());
	            pst.setInt(6, pedido.getCliente().getId());
	            pst.setInt(7, pedido.getEndereco().getId());
	            pst.setString(8, "ATIVO");
	            
	            pst.executeUpdate();

	            ResultSet rs = pst.getGeneratedKeys();  // Gera os id automaticamente
	            int idPedido = 0;
	            
	            if (rs.next()) {
	                idPedido = rs.getInt(1);
	            }
	            
	            pedido.setId(idPedido);

	            for (Manga manga : pedido.getMangas().getMangas()) {
	            	
	                sql = new StringBuilder();
	                
	                sql.append("INSERT INTO pedidos_manga(");
	                sql.append("ped_mg_id");
	                sql.append("ped_mg_mg_id");
	                sql.append("ped_mg_valor");
	                sql.append("ped_mg_qtde");
	                sql.append("ped_mg_status");
	                sql.append("ativo");
	                sql.append(") VALUES (?, ?, ?, ?, ?, ?)");
	                try {
	                    pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primarias geradas
	                    pst.setInt(1, pedido.getId());
	                    pst.setInt(2, manga.getId());
	                    pst.setDouble(3, manga.getPreco());
	                    pst.setInt(4, manga.getQuantidadeProdutoCarrinho());
	                    pst.setString(5, "EM PROCESSAMENTO");
	                    pst.setString(6, "ATIVO");

	                    pst.executeUpdate();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }

	                sql = new StringBuilder();
	                if ((manga.getQuantidade() - manga.getQuantidadeProdutoCarrinho() <= 0)) {
	                    sql.append("UPDATE estoque SET etq_quantidade = 0 WHERE etq_mg_id = '" + manga.getId() + "';");
	                } else {
	                    sql.append("UPDATE estoque SET etq_quantidade = " + (manga.getQuantidade() - manga.getQuantidadeProdutoCarrinho()) + " WHERE etq_mg_id = '" + manga.getId() + "';");
	                }
	                try {
	                    pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);// retorna as chaves primarias geradas                    
	                    pst.executeUpdate();

	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }

	                // Atualiza a carteira do cliente
	                sql = new StringBuilder();
	                sql.append("UPDATE `loja_manga`.`cliente` SET `cli_carteira`= " + pedido.getCliente().getCarteira() + " WHERE cli_id = '" + pedido.getCliente().getId() + "';");
	                try {
	                    pst = conexao.prepareStatement(sql.toString());
	                    pst.executeUpdate();

	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	            
	            conexao.close();

	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
			return resultado;
	    }

	    @Override
	    public Resultado alterar(EntidadeDominio entidadeDominio) {
	    	
	        abrirConexao();
	        Resultado resultado = new Resultado();
	        Pedido pedido = (Pedido) entidadeDominio;

	        PreparedStatement pst = null;
	        StringBuilder sql = new StringBuilder();
	        
	        
	        sql.append("UPDATE " + nomeTabela + " SET ");
	        sql.append("ped_status = ? ");
	        sql.append("WHERE " + idTabela + " = " + pedido.getId());

	        try {
	            pst = conexao.prepareStatement(sql.toString());

	            pst.setString(1, pedido.getStatus());
	            pst.executeUpdate();
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }	        
	        
			return resultado;
	    }

	    @Override
	    public Resultado consultar(EntidadeDominio entidadeDominio) {
	    	
	    	Resultado resultado = new Resultado();
	        ArrayList<Manga> mangas = new ArrayList<Manga>();
	        Pedido pedido = (Pedido) entidadeDominio;
	        ClienteDAO clienteDAO = new ClienteDAO();
	        MangaDAO mangaDao = new MangaDAO();
	        EnderecoDAO enderecosDAO = new EnderecoDAO();
	        CartaoCreditoDAO cartoesDAO = new CartaoCreditoDAO();

	        ResultSet rsPedidos;

	        abrirConexao();

	        PreparedStatement pst = null;
	        StringBuilder sql = new StringBuilder();

	        if (pedido.getStatus().equals("meuspedidos")){
	        	System.out.println("- meuspedidos");
	            sql.append("SELECT * ");
	            sql.append("FROM " + nomeTabela);
	            sql.append(" WHERE ped_cli_id = '" + pedido.getCliente().getId() + "'");
	            sql.append(" GROUP BY ped_id;");
	        } else if (pedido.getStatus().equals("todospedidos")) {
	        	System.out.println("- todospedidos");
	            sql.append("SELECT *");
	            sql.append("FROM " + nomeTabela);
	            sql.append(";");
	        } else {
	            sql.append("SELECT * ");
	            sql.append("FROM " + nomeTabela);
	            sql.append(" GROUP BY ped_id;");
	        }
	        
	        
	        try {
	            pst = conexao.prepareStatement(sql.toString());

	            rsPedidos = pst.executeQuery();

	            Cliente cliente = new Cliente();

	            MangasPedido produtosPedido = new MangasPedido();

	            while (rsPedidos.next()) {
	                //Objetos recriados para nao sobreescrever os valores anteriores
	            	Endereco endereco = new Endereco();
	                CartaoCredito card = new CartaoCredito();

	                pedido = new Pedido();
	                pedido.setId(rsPedidos.getInt("ped_id"));
	                
	                pedido.setValorTotal(rsPedidos.getDouble("ped_valor_total"));
	                pedido.setStatus(rsPedidos.getString("ped_status"));
	               
                    pedido.setDataPagamento(rsPedidos.getString("ped_data_pagamento"));
	                pedido.setDataPedido(rsPedidos.getString("ped_data_pedido"));

	                // Setando os ids para consultas futuras
	                endereco.setId(rsPedidos.getInt("ped_end_id"));

	                // Criando cliente para consulta
	                cliente.setId(rsPedidos.getInt("ped_cli_id"));
	                cliente.setNomeCompleto("v");
	                ClienteDAO cliDao = new ClienteDAO();
	                
	                
	                pedido.setCliente((Cliente)cliDao.consultar(cliente).getEntidades().get(0));

	                // Criando endereco para consulta
	                EnderecoDAO endDao = new EnderecoDAO();
	                
	                pedido.setEndereco((Endereco) endDao.consultar(endereco).getEntidades().get(0));

	                // Criando cartao para consulta
	                StringBuilder sqlCartao = new StringBuilder();
	                
	                CartaoCreditoDAO cardDao = new CartaoCreditoDAO();
	                
	                // Criando produtos para salvar na tabela MangasPedido
	                StringBuilder sqlProdutos = new StringBuilder();
	                ResultSet rsProdutos;

	                int qtdeProdutoPedido;
	                double valorOriginalProduto = 0;

	                sqlProdutos.append("SELECT * ");
	                sqlProdutos.append("FROM pedidos_mangas");
	                sqlProdutos.append(" WHERE ped_mg_id = " + pedido.getId());

	                try {
	                    pst = conexao.prepareStatement(sqlProdutos.toString());

	                    rsProdutos = pst.executeQuery();

	                    // Verifica a quantidade de produtos que o produto tinha
	                    while (rsProdutos.next()) {
	                        Manga manga = new Manga();
	                        manga.setId(rsProdutos.getInt("ped_mg_mg_id"));
	                        qtdeProdutoPedido = rsProdutos.getInt("ped_mg_qtde");

	                        // Atualiza o preco do produto, pois ele pode ter sido alterado depois da venda, e salva a quantidade de cada                       
	                        manga.setQuantidadeProdutoCarrinho(qtdeProdutoPedido);
	                        manga.setPreco(valorOriginalProduto);

	                        

	                        mangas.add(manga);
	                    }
	                } catch (SQLException ex) {
	                    ex.printStackTrace();
	                }
	                produtosPedido.setMangas(mangas);
	                pedido.setMangas(produtosPedido);
	                resultado.add(pedido);
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        return resultado;
	    }

}
