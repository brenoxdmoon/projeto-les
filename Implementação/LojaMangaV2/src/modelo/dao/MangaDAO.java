package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import modelo.dominio.Categoria;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.Resultado;

public class MangaDAO extends AbstractDAO {
	
	public MangaDAO(Connection conexao, String nomeTabela, String idTabela) {
        super(conexao, "manga", "mg_id");
    }

    public MangaDAO(String nomeTabela, String idTabela) {
        super("manga", "mg_id");
    }

    public MangaDAO() {
        super("manga", "mg_id");
    }

	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
		Resultado resultado = new Resultado();
		
		PreparedStatement pst = null;
        Manga manga = (Manga) entidadeDominio;
        CategoriaDAO catDao = new CategoriaDAO();
        EstoqueDAO etqDao = new EstoqueDAO();
        StringBuilder sql = new StringBuilder();
		
        System.out.println(manga.getTitulo());

        sql.append("INSERT INTO " + nomeTabela	+ "(");
        
        sql.append("mg_autor, ");
        sql.append("mg_ano, ");
        sql.append("mg_titulo, ");
        sql.append("mg_editora, ");
        sql.append("mg_edicao, ");
        
        sql.append("mg_isbn, ");
        sql.append("mg_paginas,");
        sql.append("mg_sinopse, ");
        sql.append("mg_altura, ");
        sql.append("mg_largura, ");
        
        sql.append("mg_profundidade, ");
        sql.append("mg_peso, ");
        sql.append("mg_cod_barras, ");
        sql.append("mg_grupo_precificacao, ");
        sql.append("ativo, mg_quantidade, ");
        
        sql.append("mg_preco, ");
        sql.append("mg_motivo_inativ,");
        sql.append("mg_motivo_ativ, ");
        sql.append("mg_imagem");
        
        sql.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        try {
            pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
            
            pst.setString(1,  manga.getAutor());
            pst.setInt	 (2,  manga.getAno());
            pst.setString(3,  manga.getTitulo());
            pst.setString(4,  manga.getEditora());
            pst.setInt	 (5,  manga.getEdicao());
            pst.setString(6,  manga.getIsbn());
            pst.setInt	 (7,  manga.getPaginas());
            pst.setString(8,  manga.getSinopse());
            pst.setDouble(9,  manga.getAltura());
            pst.setDouble(10, manga.getLargura());
            pst.setDouble(11, manga.getProfundidade());
            pst.setDouble(12, manga.getPeso());
            pst.setString(13, manga.getCodigoBarras());
            
            pst.setString(14, manga.getGrupoPrecificacao());
            
            pst.setString(15, manga.getAtivo());
            pst.setInt	 (16, manga.getQuantidade());
            pst.setDouble(17, manga.getPreco());
            pst.setString(18, manga.getMotivoInativacao());
            pst.setString(19, manga.getMotivoAtivacao());
            pst.setString(20, manga.getEndereco_imagem());
            
            
            pst.executeUpdate();
            conexao.commit();

            ResultSet rs = pst.getGeneratedKeys();  // Gera os id automaticamente
            int idManga = 0;
            if (rs.next()) {
            	idManga = rs.getInt(1);
            }
            manga.setId(idManga);
            
            
            
            //atribui o ID Manga retornado ao endere�o cadastrado para salvar em BD
            manga.getCategorias().get(0).setId(manga.getId());
            
            //Salva Categoria com ID de Manga
            catDao.salvar(manga.getCategorias().get(0));
            
            
            //salva Produto em Estoque
            etqDao.salvar(manga);
            
			resultado.add(manga);
            conexao.close();

        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
        System.out.println("- salvou");
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();

    	Resultado resultado = new Resultado();
    	
    	Manga manga;
        try {
            manga = (Manga) entidadeDominio;
        } catch (ClassCastException e) {
            manga = new Manga();
            manga.setAtivo("ATIVO");
        }
        
        PreparedStatement pst = null;
    	StringBuilder sql = new StringBuilder();
    	System.out.println(manga.getTitulo());

        sql.append("UPDATE " + nomeTabela + " SET ");
        
        sql.append("mg_autor = ?, ");
        sql.append("mg_ano = ?, ");
        sql.append("mg_titulo = ?, ");
        sql.append("mg_editora = ?, ");
        sql.append("mg_edicao = ?, ");
        sql.append("mg_isbn = ?, ");
        sql.append("mg_paginas = ?, ");
        sql.append("mg_sinopse = ?, ");
        sql.append("mg_altura = ?, ");
        sql.append("mg_largura = ?, ");
        sql.append("mg_profundidade = ?, ");
        sql.append("mg_peso = ?, ");
        sql.append("mg_cod_barras = ?, ");
        sql.append("mg_grupo_precificacao = ?, ");
        sql.append("mg_quantidade = ?, ");
        sql.append("mg_preco = ?, ");
        sql.append("mg_motivo_inativ = ?, ");
        sql.append("mg_motivo_ativ = ?, ");
        sql.append("mg_imagem = ? ");
        
        sql.append("WHERE (" + idTabela + " = " + manga.getId() + ");");

        try {
            pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
            
            pst.setString(1,  manga.getAutor());
            pst.setInt	 (2,  manga.getAno());
            pst.setString(3,  manga.getTitulo());
            pst.setString(4,  manga.getEditora());
            pst.setInt	 (5,  manga.getEdicao());
            pst.setString(6,  manga.getIsbn());
            pst.setInt	 (7,  manga.getPaginas());
            pst.setString(8,  manga.getSinopse());
            pst.setDouble(9,  manga.getAltura());
            pst.setDouble(10, manga.getLargura());
            pst.setDouble(11, manga.getProfundidade());
            pst.setDouble(12, manga.getPeso());
            pst.setString(13, manga.getCodigoBarras());
            pst.setString(14, manga.getGrupoPrecificacao());
            pst.setInt	 (15, manga.getQuantidade());
            pst.setDouble(16, manga.getPreco());
            pst.setString(17, manga.getMotivoInativacao());
            pst.setString(18, manga.getMotivoAtivacao());
            pst.setString(19, manga.getEndereco_imagem());
            
            System.out.println("MangaDAO: entdom id = " + manga.getEndereco_imagem());
            
            pst.executeUpdate();
            conexao.commit();
            
            System.out.println("- Alterou");
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
    	Resultado resultado = new Resultado();
        Manga manga = (Manga) entidadeDominio;
        CategoriaDAO categoriasDAO = new CategoriaDAO();
        

        ResultSet rs;

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        
        if(manga.getId() != -1) {
        	sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
            sql.append(" WHERE mg_id = " + manga.getId());
        } else {
        	sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
        }
         
        try {
            pst = conexao.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            manga = null;

            while (rs.next()) {
            	
            	//Falta categorias
                manga = new Manga(rs.getString("mg_autor"), rs.getString("mg_titulo"), rs.getString("mg_editora"), rs.getString("mg_sinopse"),
                		rs.getString("mg_grupo_precificacao"), rs.getString("ativo"), rs.getString("mg_motivo_ativ"), rs.getString("mg_motivo_inativ"),
                		rs.getInt("mg_ano"), rs.getInt("mg_edicao"), rs.getString("mg_isbn"), rs.getInt("mg_paginas"), rs.getString("mg_cod_barras"),
                		rs.getInt("mg_quantidade"), rs.getDouble("mg_altura"), rs.getDouble("mg_largura"), rs.getDouble("mg_profundidade"),
                		rs.getDouble("mg_peso"), rs.getDouble("mg_preco"), 0, rs.getString("mg_imagem"));
                manga.setId(rs.getInt("mg_id"));
                
                ArrayList<Categoria> categorias = new ArrayList<Categoria>();
                Categoria categoria = new Categoria();
                categoria.setMangas(manga);
                
				/*for (EntidadeDominio e : categoriasDAO.consultar(categoria).getEntidades()) {
                	categorias.add((Categoria)e);
                }*/
                
                // Salvando as consultas de categoria no manga
                manga.setCategorias(categorias);

                resultado.add(manga);
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        return resultado;
    }
}
