package modelo.dao;

import java.util.List;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public interface IDAO {

    public Resultado salvar(EntidadeDominio entidadeDominio);

    public Resultado alterar(EntidadeDominio entidadeDominio);

    public Resultado excluir(EntidadeDominio entidadeDominio);

    public Resultado consultar(EntidadeDominio entidadeDominio);

}
