package modelo.dominio;

import java.util.ArrayList;

public class Categoria extends EntidadeDominio{
	private String categoria;
	private Manga mangas;
	
	public Categoria (String categoria, Manga mangas) {
		this.categoria = categoria;
		this.mangas = mangas;
	}
	
	public Categoria () {
		System.out.println("Categoria gerada vazia");
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Manga getMangas() {
		return mangas;
	}

	public void setMangas(Manga mangas) {
		this.mangas = mangas;
	}	
	
}
