package modelo.dominio;

import java.util.ArrayList;

public class Manga extends EntidadeDominio {

	private String autor;
	private String titulo;
	private String editora;
	private String sinopse;
	private String grupoPrecificacao;
	private String ativo;
	private String motivoAtivacao;
	private String motivoInativacao;
	private int ano;
	private int edicao;
	private String isbn;
	private int paginas;
	private String codigoBarras;
	private int quantidade;
	private double altura;
	private double largura;
	private double profundidade;
	private double peso;
	private double preco;
	private int quantidadeProdutoCarrinho;
	private ArrayList<Categoria> categorias = new ArrayList<Categoria>();
	private String endereco_imagem;

	public Manga(String autor, String titulo, String editora, String sinopse, String grupoPrecificacao, String ativo,
			String motivoAtivacao, String motivoInativacao, int ano, int edicao, String isbn, int paginas,
			String codigoBarras, int quantidade, double altura, double largura, double profundidade, double peso,
			double preco, ArrayList<Categoria> categorias, int quantidadeProdutoCarrinho) {
		this.autor = autor;
		this.titulo = titulo;
		this.editora = editora;
		this.sinopse = sinopse;
		this.grupoPrecificacao = grupoPrecificacao;
		this.ativo = ativo;
		this.motivoAtivacao = motivoAtivacao;
		this.motivoInativacao = motivoInativacao;
		this.ano = ano;
		this.edicao = edicao;
		this.isbn = isbn;
		this.paginas = paginas;
		this.codigoBarras = codigoBarras;
		this.quantidade = quantidade;
		this.altura = altura;
		this.largura = largura;
		this.profundidade = profundidade;
		this.peso = peso;
		this.preco = preco;
		this.categorias = categorias;
		this.quantidadeProdutoCarrinho = quantidadeProdutoCarrinho;
	}

	// Manga sem categorias
	public Manga(String autor, String titulo, String editora, String sinopse, String grupoPrecificacao, String ativo,
			String motivoAtivacao, String motivoInativacao, int ano, int edicao, String isbn, int paginas,
			String codigoBarras, int quantidade, double altura, double largura, double profundidade, double peso,
			double preco, int quantidadeProdutoCarrinho) {
		this.autor = autor;
		this.titulo = titulo;
		this.editora = editora;
		this.sinopse = sinopse;
		this.grupoPrecificacao = grupoPrecificacao;
		this.ativo = ativo;
		this.motivoAtivacao = motivoAtivacao;
		this.motivoInativacao = motivoInativacao;
		this.ano = ano;
		this.edicao = edicao;
		this.isbn = isbn;
		this.paginas = paginas;
		this.codigoBarras = codigoBarras;
		this.quantidade = quantidade;
		this.altura = altura;
		this.largura = largura;
		this.profundidade = profundidade;
		this.peso = peso;
		this.preco = preco;
		this.quantidadeProdutoCarrinho = quantidadeProdutoCarrinho;
	}

	public Manga(String autor, String titulo, String editora, String sinopse, String grupoPrecificacao, String ativo,
			String motivoAtivacao, String motivoInativacao, int ano, int edicao, String isbn, int paginas,
			String codigoBarras, int quantidade, double altura, double largura, double profundidade, double peso,
			double preco, int quantidadeProdutoCarrinho, String endereco_imagem) {
		this.autor = autor;
		this.titulo = titulo;
		this.editora = editora;
		this.sinopse = sinopse;
		this.grupoPrecificacao = grupoPrecificacao;
		this.ativo = ativo;
		this.motivoAtivacao = motivoAtivacao;
		this.motivoInativacao = motivoInativacao;
		this.ano = ano;
		this.edicao = edicao;
		this.isbn = isbn;
		this.paginas = paginas;
		this.codigoBarras = codigoBarras;
		this.quantidade = quantidade;
		this.altura = altura;
		this.largura = largura;
		this.profundidade = profundidade;
		this.peso = peso;
		this.preco = preco;
		this.quantidadeProdutoCarrinho = quantidadeProdutoCarrinho;
		this.endereco_imagem = endereco_imagem;
	}

	public Manga() {
		System.out.println("Manga gerado vazio");
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public String getGrupoPrecificacao() {
		return grupoPrecificacao;
	}

	public void setGrupoPrecificacao(String grupoPrecificacao) {
		this.grupoPrecificacao = grupoPrecificacao;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public String getMotivoAtivacao() {
		return motivoAtivacao;
	}

	public void setMotivoAtivacao(String motivoAtivacao) {
		this.motivoAtivacao = motivoAtivacao;
	}

	public String getMotivoInativacao() {
		return motivoInativacao;
	}

	public void setMotivoInativacao(String motivoInativacao) {
		this.motivoInativacao = motivoInativacao;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getEdicao() {
		return edicao;
	}

	public void setEdicao(int edicao) {
		this.edicao = edicao;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getPaginas() {
		return paginas;
	}

	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) {
		this.largura = largura;
	}

	public double getProfundidade() {
		return profundidade;
	}

	public void setProfundidade(double profundidade) {
		this.profundidade = profundidade;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public ArrayList<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(ArrayList<Categoria> categorias) {
		this.categorias = categorias;
	}

	public String getEndereco_imagem() {
		return endereco_imagem;
	}

	public void setEndereco_imagem(String endereco_imagem) {
		this.endereco_imagem = endereco_imagem;
	}

	public int getQuantidadeProdutoCarrinho() {
		return quantidadeProdutoCarrinho;
	}

	public void setQuantidadeProdutoCarrinho(int quantidadeProdutoCarrinho) {
		this.quantidadeProdutoCarrinho = quantidadeProdutoCarrinho;
	}

}
