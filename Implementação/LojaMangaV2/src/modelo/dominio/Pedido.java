package modelo.dominio;

public class Pedido extends EntidadeDominio {
	
    private CartaoCredito cartao;
    private double valorTotal;
    private String dataPagamento;
    private String dataPedido;
    private String status;
    private Cliente cliente;
    private Endereco endereco;
    private MangasPedido mangas;
    private String ativo;
    //private MetodoEntrega metodoEntrega;

    public Pedido() {

    }
    
    // Construtor para pedido no cartao
    public Pedido(String tipoPagamento, CartaoCredito cartao, double valorTotal, String dataPagamento, String dataPedido, String status, Cliente cliente, Endereco endereco, MangasPedido mangas) {

        this.cartao = cartao;
        this.valorTotal = valorTotal;
        this.dataPagamento = dataPagamento;
        this.dataPedido = dataPedido;
        this.status = status;
        this.cliente = cliente;
        this.endereco = endereco;
        this.mangas = mangas;
        //this.setMetodoEntrega(metodoEntrega);
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(String dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public String getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public CartaoCredito getCartao() {
        return cartao;
    }

    public void setCartao(CartaoCredito cartao) {
        this.cartao = cartao;
    }   

    public MangasPedido getMangas() {
        return mangas;
    }

    public void setMangas(MangasPedido mangas) {
        this.mangas = mangas;
    }

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

//	public MetodoEntrega getMetodoEntrega() {
//		return metodoEntrega;
//	}
//
//	public void setMetodoEntrega(MetodoEntrega metodoEntrega) {
//		this.metodoEntrega = metodoEntrega;
//	}
	
}
