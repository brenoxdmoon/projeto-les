package view.command;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public interface ICommand {

	public Resultado executar(EntidadeDominio ent);
}
