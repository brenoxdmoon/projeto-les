package view.command;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public class SalvarCommand extends AbstractCommand {

    @Override
    public Resultado executar(EntidadeDominio entidadeDominio) {
        return fachada.salvar(entidadeDominio);
    }

    
}
