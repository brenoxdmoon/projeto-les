
package view.command;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public class AlterarCommand extends AbstractCommand {

    @Override
    public Resultado executar(EntidadeDominio entidadeDominio) {
        return fachada.alterar(entidadeDominio);
    }
}
