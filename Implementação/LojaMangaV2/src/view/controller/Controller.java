package view.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.dominio.Cliente;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;
import view.command.AlterarCommand;
import view.command.ConsultarCommand;
import view.command.ExcluirCommand;
import view.command.ICommand;
import view.command.SalvarCommand;
import view.command.VisualizarCommand;
import view.viewHelper.CarrinhoVH;
import view.viewHelper.CartaoCreditoVH;
import view.viewHelper.ClienteVH;
import view.viewHelper.EnderecoVH;
import view.viewHelper.IViewHelper;
import view.viewHelper.MangaVH;
import view.viewHelper.PedidoVH;


public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Map<String, IViewHelper> vhs;
	Map<String, ICommand> cmd;
	
	
    public Controller() {
    	
    	cmd = new HashMap<String, ICommand>();
    	
    	cmd.put("SALVAR", new SalvarCommand());
    	cmd.put("ALTERAR", new AlterarCommand());
    	cmd.put("CONSULTAR", new ConsultarCommand());
    	cmd.put("EXCLUIR", new ExcluirCommand());
    	cmd.put("VISUALIZAR", new VisualizarCommand());
    	cmd.put("LOGIN", new VisualizarCommand());
    	
    	vhs = new HashMap<String, IViewHelper>();
    	
    	vhs.put("/LojaMangaV2/SalvarCliente", new ClienteVH());
    	vhs.put("/LojaMangaV2/ConsultarCliente", new ClienteVH());
    	vhs.put("/LojaMangaV2/AlterarCliente", new ClienteVH());
    	vhs.put("/LojaMangaV2/ExcluirCliente", new ClienteVH());
    	vhs.put("/LojaMangaV2/VisualizarCliente", new ClienteVH());
    	vhs.put("/LojaMangaV2/Login", new ClienteVH());
    	
    	vhs.put("/LojaMangaV2/AlterarEndereco", new EnderecoVH());
    	vhs.put("/LojaMangaV2/SalvarEndereco", new EnderecoVH());
    	
    	vhs.put("/LojaMangaV2/SalvarCartao", new CartaoCreditoVH());
    	vhs.put("/LojaMangaV2/AlterarCartao", new CartaoCreditoVH());
    	
    	vhs.put("/LojaMangaV2/SalvarManga", new MangaVH());
    	vhs.put("/LojaMangaV2/ConsultarManga", new MangaVH());
    	vhs.put("/LojaMangaV2/AlterarManga", new MangaVH());
    	vhs.put("/LojaMangaV2/ExcluirManga", new MangaVH());
    	vhs.put("/LojaMangaV2/VisualizarManga", new MangaVH());
    	
    	vhs.put("/LojaMangaV2/AdicionarItemCarrinho", new CarrinhoVH());

    	vhs.put("/LojaMangaV2/SalvarPedido", new PedidoVH());
    	vhs.put("/LojaMangaV2/ConsultarPedido", new PedidoVH());
    	vhs.put("/LojaMangaV2/AlterarPedido", new PedidoVH());
    	vhs.put("/LojaMangaV2/ExcluirPedido", new PedidoVH());
    	vhs.put("/LojaMangaV2/VisualizarPedido", new PedidoVH());
    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacao = request.getParameter("OPERACAO");
		String uri = request.getRequestURI();
		
		Resultado resultado = new Resultado();
		
		IViewHelper vh = vhs.get(uri);
		ICommand command = null;
		
		EntidadeDominio ent = vh.getEntidade(request);
		
		command = cmd.get(operacao);
		
		resultado = command.executar(ent);
		
		vh.setView(resultado, request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String operacao = request.getParameter("OPERACAO");
		String uri = request.getRequestURI();
		
		Resultado resultado = new Resultado();
		
		IViewHelper vh = vhs.get(uri);
		ICommand command = null;
		
		EntidadeDominio ent = vh.getEntidade(request);
		
		command = cmd.get(operacao);
		
		resultado = command.executar(ent);
		
		vh.setView(resultado, request, response);
		
	}
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher rd = null;
		rd = request.getRequestDispatcher("login.jsp");
		request.getSession().setAttribute("clienteLogado", null);
		
		rd.forward(request, response);
	}

}
