package view.viewHelper;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.dao.MangaDAO;
import modelo.dominio.Categoria;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.Resultado;

public class MangaVH implements IViewHelper {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		
		HttpSession session = null;
		Manga mg = new Manga();

		if (request.getParameter("idMg") != null && !request.getParameter("idMg").trim().equals("")) {

			mg.setId(Integer.parseInt(request.getParameter("idMg")));

		} else {
			mg.setId(0);
		}

		String operacao = request.getParameter("OPERACAO");

		if (operacao.equals("CONSULTAR")) {

			mg.setId(-1);

			session = request.getSession();
			request.setAttribute("manga", mg);

		} else if (operacao.equals("SALVAR")) {

			session = request.getSession();
			mg = buildMangaSalvar(request);

		} else if (operacao.equals("ALTERAR")) {
			
			System.out.println("- VH Alterar");

			mg = buildMangaAlterar(request);

		} else if (operacao.equals("EXCLUIR")) {

			session = request.getSession();
			mg = new Manga();
			mg.setId(Integer.parseInt(request.getParameter("idMg")));

		} else if (operacao.equals("VISUALIZAR")) {

			session = request.getSession();
			mg.setId(Integer.valueOf(request.getParameter("idMg")));
			System.out.println("id de mang� para visualizar"+mg.getId());
			mg.setTitulo("");
			request.setAttribute("VisualizarMg", mg);

		}

		return mg;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		Manga mg = new Manga();
		RequestDispatcher rd = null;

		try {
			mg = (Manga) resultado.getEntidades().get(0);
		} catch (Exception e) {
			mg = new Manga();
		}

		String operacao = request.getParameter("OPERACAO");

		if (request.getParameter("idMg") != null && !request.getParameter("idMg").trim().equals("")) {

			mg.setId(Integer.parseInt(request.getParameter("idMg")));

		} else {
			mg.setId(0);
		}

		if (operacao.equals("CONSULTAR")) {

			mg = (Manga) resultado.getEntidades().get(0);
			request.getSession().setAttribute("resultado", resultado);
			request.getSession().setAttribute("manga", mg);
			rd = request.getRequestDispatcher("autenticado/admin/manga-list.jsp");

		} else if (operacao.equals("SALVAR")) {

			MangaDAO mgDao = new MangaDAO();
			resultado = mgDao.consultar(mg);
			request.getSession().setAttribute("resultado", resultado);
			request.getSession().setAttribute("manga", mg);
			rd = request.getRequestDispatcher("autenticado/admin/manga-list.jsp");

		}
		if (operacao.equals("VISUALIZAR")) {

			mg = (Manga) resultado.getEntidades().get(0);
			request.getSession(true).setAttribute("VisualizarMg", mg);

			System.out.println("- Redirecionando para p�gina visualizar");
			
			if(request.getParameter("PAGINA").equals("cliente")) {
				
				rd = request.getRequestDispatcher("manga-view.jsp");
				
			}else if(request.getParameter("PAGINA").equals("admin")) {
				
				rd = request.getRequestDispatcher("autenticado/admin/manga-view.jsp");	
				
			}
		}
		if (operacao.equals("ALTERAR")) {

			MangaDAO mgDao = new MangaDAO();
			mg = (Manga) resultado.getEntidades().get(0);
			request.getSession(true).setAttribute("AlterarMg", mg);

			mg.setId(-1);
			resultado = mgDao.consultar(mg);

			request.getSession().setAttribute("resultado", resultado);

			System.out.println("- Redirecionando para p�gina alterar");

			rd = request.getRequestDispatcher("autenticado/admin/manga-list.jsp");
		}
		if (operacao.equals("EXCLUIR")) {

			rd = request.getRequestDispatcher("autenticado/admin/manga-list.jsp");

		}

		rd.forward(request, response);
	}

	private Manga buildMangaSalvar(HttpServletRequest request) {

		Manga manga = new Manga();

		// Criando Categoria
		String cate = request.getParameter("txtCategoria");

		// Criando Manga
		String autor = request.getParameter("txtAutor");
		String titulo = request.getParameter("txtTitulo");
		String editora = request.getParameter("txtEditora");
		String sinopse = request.getParameter("txtSinopse");
		String grupoPrecificacao = request.getParameter("txtPrecificacao");
		String motivoAtivacao = request.getParameter("txtMotivoAtivacao");
		String motivoInativacao = request.getParameter("txtMotivoInativacao");
		int ano = Integer.valueOf(request.getParameter("txtAno"));
		int edicao = Integer.valueOf(request.getParameter("txtEdicao"));
		String isbn = request.getParameter("txtIsbn");
		int paginas = Integer.valueOf(request.getParameter("txtPaginas"));
		String codigoBarras = request.getParameter("txtCodigoBarras");
		int quantidade = Integer.valueOf(request.getParameter("txtQuantidade"));
		double altura = Double.valueOf(request.getParameter("txtAltura"));
		double largura = Double.valueOf(request.getParameter("txtLargura"));
		double profundidade = Double.valueOf(request.getParameter("txtProfundidade"));
		double peso = Double.valueOf(request.getParameter("txtPeso"));
		double preco = Double.valueOf(request.getParameter("txtPreco"));
		String imagem = request.getParameter("mgImagem");
		String ativo = "ATIVO";

		// Atribuindo dados de categoria
		if (cate == null || cate.trim().equals("")) {
			cate = "";
		}

		// Atribuindo dados de Manga
		if (autor != null) {
			manga.setAutor(autor);
		} else {
			manga.setAutor("");
		}

		if (titulo != null) {
			manga.setTitulo(titulo);
		} else {
			manga.setTitulo("");
		}

		if (editora != null) {
			manga.setEditora(editora);
		} else {
			manga.setEditora("");
		}

		if (sinopse != null) {
			manga.setSinopse(sinopse);
		} else {
			manga.setSinopse("");
		}

		if (grupoPrecificacao != null) {
			manga.setGrupoPrecificacao(grupoPrecificacao);
		} else {
			manga.setGrupoPrecificacao("");
		}

		if (motivoAtivacao != null) {
			manga.setMotivoAtivacao(motivoAtivacao);
		} else {
			manga.setMotivoAtivacao("");
		}

		if (motivoInativacao != null) {
			manga.setMotivoInativacao(motivoInativacao);
		} else {
			manga.setMotivoInativacao("");
		}

		if (imagem != null) {
			manga.setEndereco_imagem(imagem);
		} else {
			manga.setEndereco_imagem("");
		}

		// N�o sei oq fazer se for null
		manga.setAno(ano);
		manga.setEdicao(edicao);
		manga.setIsbn(isbn);
		manga.setPaginas(paginas);
		manga.setCodigoBarras(codigoBarras);
		manga.setQuantidade(quantidade);
		manga.setLargura(largura);
		manga.setAltura(altura);
		manga.setProfundidade(profundidade);
		manga.setPeso(peso);
		manga.setPreco(preco);

		// Atribuindo categoria � Mang�
		Categoria categoria = new Categoria();
		categoria.setCategoria(cate);

		ArrayList<Categoria> categorias = new ArrayList();
		categorias.add(categoria);

		manga.setCategorias(categorias);

		// Atribuindo Status a Mang�
		manga.setAtivo(ativo);

		System.out.println("- Entidade Manga montada com sucesso no VH");
		return manga;
	}

	private Manga buildMangaAlterar(HttpServletRequest request) {

		Manga manga = new Manga();

		// Criando Manga
		String autor = request.getParameter("txtAutor");
		String titulo = request.getParameter("txtTitulo");
		String editora = request.getParameter("txtEditora");
		String sinopse = request.getParameter("txtSinopse");
		String grupoPrecificacao = request.getParameter("txtPrecificacao");
		String motivoAtivacao = request.getParameter("txtMotivoAtivacao");
		String motivoInativacao = request.getParameter("txtMotivoInativacao");
		int ano = Integer.valueOf(request.getParameter("txtAno"));
		int edicao = Integer.valueOf(request.getParameter("txtEdicao"));
		String isbn = request.getParameter("txtIsbn");
		int paginas = Integer.valueOf(request.getParameter("txtPaginas"));
		String codigoBarras = request.getParameter("txtCodigoBarras");
		int quantidade = Integer.valueOf(request.getParameter("txtQuantidade"));
		double altura = Double.valueOf(request.getParameter("txtAltura"));
		double largura = Double.valueOf(request.getParameter("txtLargura"));
		double profundidade = Double.valueOf(request.getParameter("txtProfundidade"));
		double peso = Double.valueOf(request.getParameter("txtPeso"));
		double preco = Double.valueOf(request.getParameter("txtPreco"));
		String imagem = request.getParameter("mgImagem");
		String ativo = "ATIVO";

		int id = Integer.valueOf(request.getParameter("idMg"));

		if (autor != null) {
			manga.setAutor(autor);
		} else {
			manga.setAutor("");
		}

		if (titulo != null) {
			manga.setTitulo(titulo);
		} else {
			manga.setTitulo("");
		}

		if (editora != null) {
			manga.setEditora(editora);
		} else {
			manga.setEditora("");
		}

		if (sinopse != null) {
			manga.setSinopse(sinopse);
		} else {
			manga.setSinopse("");
		}

		if (grupoPrecificacao != null) {
			manga.setGrupoPrecificacao(grupoPrecificacao);
		} else {
			manga.setGrupoPrecificacao("");
		}

		if (motivoAtivacao != null) {
			manga.setMotivoAtivacao(motivoAtivacao);
		} else {
			manga.setMotivoAtivacao("");
		}

		if (motivoInativacao != null) {
			manga.setMotivoInativacao(motivoInativacao);
		} else {
			manga.setMotivoInativacao("");
		}

		if (isbn != null) {
			manga.setIsbn(isbn);
		} else {
			manga.setIsbn("");
		}

		if (codigoBarras != null) {
			manga.setCodigoBarras(codigoBarras);
		} else {
			manga.setCodigoBarras("");
		}

		if (imagem != null) {
			System.out.println("- texto da imagem: " + manga.getEndereco_imagem());
			manga.setEndereco_imagem(imagem);
		} else {
			manga.setEndereco_imagem("");
		}

		// N�o sei oq fazer se for null
		manga.setAno(ano);
		manga.setEdicao(edicao);
		manga.setPaginas(paginas);
		manga.setQuantidade(quantidade);
		manga.setLargura(largura);
		manga.setAltura(altura);
		manga.setProfundidade(profundidade);
		manga.setPeso(peso);
		manga.setPreco(preco);

		manga.setAtivo(ativo);

		manga.setId(id);
		System.out.println("MangaVH: Ano que est� sendo colocado no Alterar: " + manga.getAno());
		System.out.println("- Entidade Manga montada com sucesso no VH");

		return manga;
	}

}