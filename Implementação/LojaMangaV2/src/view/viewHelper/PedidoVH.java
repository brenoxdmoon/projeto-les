package view.viewHelper;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.dominio.Carrinho;
import modelo.dominio.CartaoCredito;
import modelo.dominio.Cliente;
import modelo.dominio.Endereco;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.MangasPedido;
import modelo.dominio.Pedido;
import modelo.dominio.Resultado;

public class PedidoVH implements IViewHelper {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		System.out.println("- Entrou no PedidoVH");
		HttpSession session = null;
		Pedido pedido = new Pedido();
		
		if (request.getParameter("idPed") != null
				&& !request.getParameter("idPed").trim().equals("")) {
			
			pedido.setId(Integer.parseInt(request.getParameter("idPed")));
			
		}else {
			pedido.setId(0);
		}
		
		String operacao = request.getParameter("OPERACAO");
		
		if(operacao.equals("CONSULTAR")) {
			
			if (request.getParameter("idPed") != null
					&& !request.getParameter("idPed").trim().equals("")) {
				
				pedido.setId(Integer.parseInt(request.getParameter("idPed")));
				
			}else {
				pedido.setId(0);
			}
			
			session = request.getSession();
			request.setAttribute("pedido", pedido);
			
		}else if(operacao.equals("SALVAR")) {
			
			session = request.getSession();
			pedido = buildEntitySalvar(request);
			
		}else if(operacao.equals("ALTERAR")) {
			
			pedido = buildEntitySalvar(request);
			
		}else if (operacao.equals("EXCLUIR")){
			
			session = request.getSession();
			pedido = new Pedido();
			pedido.setId(Integer.parseInt(request.getParameter("idPed")));
			
		}else if(operacao.equals("VISUALIZAR")) {

			session = request.getSession();
			pedido.setId(Integer.valueOf(request.getParameter("idPed")));
			System.out.println(pedido.getId());
            pedido.setDataPagamento("Visualizar");;
            request.setAttribute("VisualizarPedido", pedido);
			
		}
		
		return pedido;
	}
	
	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		Pedido ped = new Pedido();
		RequestDispatcher rd = null;
		
		try {
			ped = (Pedido) resultado.getEntidades().get(0);
        } catch (Exception e) {
        	ped = new Pedido();
        }
		
		String operacao = request.getParameter("OPERACAO");
		
		if(operacao.equals("ALTERAR")) {
			
			rd = request.getRequestDispatcher("");
			
			ped = (Pedido) resultado.getEntidades().get(0);
			request.getSession().setAttribute("AlterarPed", ped);
			
			rd = request.getRequestDispatcher("orders-list.jsp");
			
		} else if (operacao.equals("EXCLUIR")) {
			
			Cliente cli = (Cliente) request.getSession().getAttribute("clienteLogado");
			
			if (cli.getAdmin() == 1) {
				rd = request.getRequestDispatcher("orders-list-admin.jsp");
			} else {
				rd = request.getRequestDispatcher("orders-list.jsp");
			}
			
			
		}
		
	}

	private Pedido buildEntitySalvar(HttpServletRequest request) {
		
		Carrinho carrinho = (Carrinho) request.getSession().getAttribute("carrinho");
		Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
		Pedido pedido = new Pedido();
		Endereco endereco = null;
		CartaoCredito cartao = null;
		
		String numCard = request.getParameter("cartaoPed");
		String cep = request.getParameter("endPed");
		
		
		for(CartaoCredito cc : cliente.getCartaoCredito()) {
			if(numCard == String.valueOf(cc.getId()) ) {
				cartao = new CartaoCredito();
				cartao.setAtivo("ATIVO");
				cartao.setBandeira(cc.getBandeira());
				cartao.setCliente(cc.getCliente());
				cartao.setCodigoSeguranca(cc.getCodigoSeguranca());
				cartao.setId(cc.getId());
				cartao.setNomeImpresso(cc.getNomeImpresso());
				cartao.setNumeroCartao(cc.getNumeroCartao());
				cartao.setValidade(cc.getValidade());
				break;
			}
		}
		
		for(Endereco e : cliente.getEndereco()) {
			if(cep == String.valueOf(e.getId())) {
				endereco = new Endereco();
				endereco = e;
				break;
			}
		}
		
		MangasPedido mgPed = new MangasPedido();
		mgPed.setMangas(carrinho.getMangas());
		
		
		
		pedido.setMangas(mgPed);
		pedido.setAtivo("ATIVO");
		pedido.setCartao(cartao);
		pedido.setEndereco(endereco);
		pedido.setCliente(cliente);
		
		// System.out.println("TESTE CARD VH: " +pedido.getCartao().getNomeImpresso());
		
		return pedido;
	}
	
	private Pedido buildEntityAlterar(HttpServletRequest request) {
		
		Pedido pedido = new Pedido();
		
		// Criando Pedido
	    double 	valorTotal = Double.valueOf(request.getParameter("txtValorTotal"));
	    String 	dataPagamento = request.getParameter("txtDataPagamento");
	    String 	dataPedido = request.getParameter("txtDataPedido");
	    String 	status = request.getParameter("txtStatus");
	    String 	ativo = "ATIVO";
	    
	    
	    // Garantindo n�o null
	    if (dataPagamento.trim().equals("") || dataPagamento == null) {
	    	dataPagamento = "";
        }
	    if (dataPedido.trim().equals("") || dataPedido == null) {
	    	dataPedido = "";
        }
	    if (status.trim().equals("") || status == null) {
	    	status = "";
        }
	    
	    // Atribuindo atributos
	    pedido.setValorTotal(valorTotal);
	    pedido.setDataPagamento(dataPagamento);
	    pedido.setDataPedido(dataPedido);
	    pedido.setStatus(status);
	    pedido.setAtivo(ativo);
	    
		return pedido;
	}

	

}
